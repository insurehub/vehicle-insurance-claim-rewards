FROM openjdk:11
MAINTAINER tinojobi@gmail.com
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} vehicle-insurance-claim-rewards.jar
ENTRYPOINT ["java","-jar","/vehicle-insurance-claim-rewards.jar"]