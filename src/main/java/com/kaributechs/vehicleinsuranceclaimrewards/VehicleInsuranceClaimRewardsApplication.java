package com.kaributechs.vehicleinsuranceclaimrewards;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleInsuranceClaimRewardsApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleInsuranceClaimRewardsApplication.class, args);
	}

}
