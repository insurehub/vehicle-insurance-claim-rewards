package com.kaributechs.vehicleinsuranceclaimrewards.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ClaimReward")
public class ClaimReward {
    @Id
    private String policyNo;
    @Column(name ="Insurer")
    private String insurer;
    @Column(name="VehicleRegistrationNumber")
    private String vehicleRegNo;
    @Column(name="RewardToClaim")
    private String rewardToClaim;
    @Column(name="AdditionalInformation")
    private String additionalInfo;

    public ClaimReward(){

    }

    public ClaimReward(String policyNo, String insurer, String vehicleRegNo, String rewardToClaim, String additionalInfo) {
        this.policyNo = policyNo;
        this.insurer = insurer;
        this.vehicleRegNo = vehicleRegNo;
        this.rewardToClaim = rewardToClaim;
        this.additionalInfo = additionalInfo;
    }


    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getVehicleRegNo() {
        return vehicleRegNo;
    }

    public void setVehicleRegNo(String vehicleRegNo) {
        this.vehicleRegNo = vehicleRegNo;
    }

    public String getRewardToClaim() {
        return rewardToClaim;
    }

    public void setRewardToClaim(String rewardToClaim) {
        this.rewardToClaim = rewardToClaim;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }


}
