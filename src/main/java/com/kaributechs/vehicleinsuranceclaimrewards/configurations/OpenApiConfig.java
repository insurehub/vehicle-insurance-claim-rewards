package com.kaributechs.vehicleinsuranceclaimrewards.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "Vehicle Insurance Claim Reward",
                description = "" +
                        "This is an api documentation page for Vehicle Insurance Claim Reward developed by Kaributechs",
                contact = @Contact(
                        name = "Tinotenda Jobi",
                        email = "tinojobi@gmail.com"
                )
        ),
        servers = @Server(url = "http://insurehubbackend.southafricanorth.cloudapp.azure.com:7006/")
)
public class OpenApiConfig {

}
