package com.kaributechs.vehicleinsuranceclaimrewards.controllers;

import com.kaributechs.vehicleinsuranceclaimrewards.models.ClaimReward;
import com.kaributechs.vehicleinsuranceclaimrewards.services.ClaimRewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/claimreward")
public class ClaimRewardController {

    @Autowired
    private ClaimRewardService service;

    // Create Claim
    @PostMapping("/add")
    public ClaimReward createClaim(@RequestBody ClaimReward claim){
        return service.createClaim(claim);
    }

    // Get Claim by ID
    @GetMapping("/{id}")
    public ClaimReward getClaimById(@PathVariable (value = "id")String claimId) {
        return service.getClaimById(claimId);
    }

    //Get all Claims
    @GetMapping
    public List<ClaimReward> getAllClaims(){
        return service.getAllClaims();
    }

}
