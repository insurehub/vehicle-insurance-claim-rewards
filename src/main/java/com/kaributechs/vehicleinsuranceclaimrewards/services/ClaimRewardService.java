package com.kaributechs.vehicleinsuranceclaimrewards.services;

import com.kaributechs.vehicleinsuranceclaimrewards.models.ClaimReward;
import com.kaributechs.vehicleinsuranceclaimrewards.exceptions.ResourceNotFoundException;
import com.kaributechs.vehicleinsuranceclaimrewards.repositories.ClaimRewardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClaimRewardService {
    @Autowired
    private ClaimRewardRepository repository;

    public ClaimReward createClaim(ClaimReward claim){
        return this.repository.save(claim);
    }

    public ClaimReward getClaimById(String claimId){
        return this.repository.findById(claimId)
                .orElseThrow(()-> new ResourceNotFoundException("Claim Details not found"));
    }

    public List<ClaimReward> getAllClaims(){
        return this.repository.findAll();
    }

}
