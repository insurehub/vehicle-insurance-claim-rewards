package com.kaributechs.vehicleinsuranceclaimrewards.repositories;

import com.kaributechs.vehicleinsuranceclaimrewards.models.ClaimReward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClaimRewardRepository extends JpaRepository<ClaimReward,String> {
}
